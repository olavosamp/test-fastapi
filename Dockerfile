FROM python:3.9.6

RUN apt-get update; apt-get install -y tzdata; apt-get install -y cron; apt-get install -y vim
WORKDIR /app/

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install --upgrade pip

COPY ./src/requirements.txt /app/
RUN pip install -r /app/requirements.txt

COPY src/ /app/

#RUN mkdir -p /app/logs/

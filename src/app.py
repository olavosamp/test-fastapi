from typing import List

from bson import ObjectId

from fastapi import FastAPI
from fastapi import HTTPException, Body
from fastapi.encoders import jsonable_encoder
from starlette import status
from starlette.responses import JSONResponse

import routes
from models import Publication, PublicationUpdate
from mongodb import publication_collection

app = FastAPI()


@app.get(routes.publication, response_description="List publications", response_model=List[Publication],
         response_model_exclude_none=True, response_model_exclude_unset=True)
async def list_publication():
    publication_list = await publication_collection.find().to_list(10)
    return publication_list


@app.get(routes.publication + "{id_mongo}", response_description="Get publication", response_model=Publication)
async def get_publication(id_mongo: str):
    if publication := await publication_collection.find_one(filter={"_id": ObjectId(id_mongo)}):
        return publication

    raise HTTPException(status_code=404, detail=f"Publicação ID {id_mongo} não foi encontrada.")


@app.post(routes.publication, response_description="Add new publication", response_model=Publication)
async def create_publication(publication: Publication = Body(...)):
    pub_dict = publication.dict(by_alias=True)
    new_publication = await publication_collection.insert_one(pub_dict)
    created_pub_dict = await publication_collection.find_one({"_id": new_publication.inserted_id})

    created_publication = jsonable_encoder(Publication(**created_pub_dict))
    return JSONResponse(status_code=status.HTTP_201_CREATED, content=created_publication)


@app.put(routes.publication + "{id_mongo}", response_description="Update publication", response_model=Publication)
async def update_publication(id_mongo: str, publication: PublicationUpdate = Body(...)):
    # Drop null update fields to allow partial update
    publication = {k: v for k, v in publication.dict().items() if v is not None}

    if len(publication) >= 1:
        update_result = await publication_collection.update_one({"_id": id_mongo}, {"$set": publication})

        if update_result.modified_count == 1:
            if (
                    updated_publication := await publication_collection.find_one({"_id": id_mongo})
            ) is not None:
                return updated_publication

    if (existing_publication := await publication_collection.find_one({"_id": id_mongo})) is not None:
        return existing_publication

    raise HTTPException(status_code=404, detail=f"Student {id_mongo} not found")
#
#
# @app.post(routes.student, response_description="Add new student", response_model=StudentModel)
# async def create_student(student: StudentModel = Body(...)):
#     student = jsonable_encoder(student)
#     new_student = await db_college["students"].insert_one(student)
#     created_student = await db_college["students"].find_one({"_id": new_student.inserted_id})
#     return JSONResponse(status_code=status.HTTP_201_CREATED, content=created_student)
#
#
# @app.get(routes.student, response_description="List all students", response_model=List[StudentModel])
# async def list_students():
#     students = await db_college["students"].find().to_list(1000)
#     return students
#
#
# # Function arguments that are present in the path with brackets {} are passed from the url
# # arguments that are not in the path are interpreted as query parameters
# @app.get(routes.student + "{id_mongo}", response_description="Get a single student", response_model=StudentModel)
# async def show_student(id_mongo: str):
#     if (student := await db_college["students"].find_one({"_id": id_mongo})) is not None:
#         return student
#
#     raise HTTPException(status_code=404, detail=f"Student {id_mongo} not found")
#
#
# @app.put(routes.student + "{id_mongo}", response_description="Update a student", response_model=StudentModel)
# async def update_student(id_mongo: str, student: UpdateStudentModel = Body(...)):
#     student = {k: v for k, v in student.dict().items() if v is not None}
#
#     if len(student) >= 1:
#         update_result = await db_college["students"].update_one({"_id": id_mongo}, {"$set": student})
#
#         if update_result.modified_count == 1:
#             if (
#                     updated_student := await db_college["students"].find_one({"_id": id_mongo})
#             ) is not None:
#                 return updated_student
#
#     if (existing_student := await db_college["students"].find_one({"_id": id_mongo})) is not None:
#         return existing_student
#
#     raise HTTPException(status_code=404, detail=f"Student {id_mongo} not found")
#
#
# @app.delete(routes.student + "{id_mongo}", response_description="Delete a student")
# async def delete_student(id_mongo: str):
#     delete_result = await db_college["students"].delete_one({"_id": id_mongo})
#
#     if delete_result.deleted_count == 1:
#         return JSONResponse(status_code=status.HTTP_204_NO_CONTENT)
#
#     raise HTTPException(status_code=404, detail=f"Student {id_mongo} not found")

from celery import Celery

MONGO_URI = "mongodb://root:root@172.16.16.82:27017/?authSource=admin"

app = Celery(
    'tasks',
    broker='amqp://admin:admin@localhost:5673//',
    backend='mongodb',
    result_backend=MONGO_URI,
    mongodb_backend_settings={
        'database': 'classificadorBigdata',
        'taskmeta_collection': 'testCelery',
    }
)

# app.config_from_object(celeryconfig)

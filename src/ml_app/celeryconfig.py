from config import MONGO_URI

broker = 'pyamqp://admin:admin@localhost:5673//'
backend = 'mongodb'
result_backend = MONGO_URI
mongodb_backend_settings = {
    'database': 'classificadorBigdata',
    'taskmeta_collection': 'testCelery',
}
task_routes = {
    'tasks.add': 'low-priority',
}

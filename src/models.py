from datetime import datetime
from typing import Optional, Any, List

from bson import ObjectId
from pydantic import BaseModel, Field, EmailStr


class PyObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not ObjectId.is_valid(v):
            raise ValueError("Invalid objectid")
        return ObjectId(v)

    # @classmethod
    # def __modify_schema__(cls, field_schema):
    #     field_schema.update(type="string")


class StudentModel(BaseModel):
    id: PyObjectId = Field(default_factory=PyObjectId, alias="_id")
    name: str = Field(...)
    email: EmailStr = Field(...)
    course: str = Field(...)
    gpa: float = Field(..., le=4.0)

    class Config:
        allow_population_by_field_name = True
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}
        schema_extra = {
            "example": {
                "name": "Jane Doe",
                "email": "jdoe@example.com",
                "course": "Experiments, Science, and Fashion in Nanophotonics",
                "gpa": "3.0",
            }
        }


class UpdateStudentModel(BaseModel):
    name: Optional[str]
    email: Optional[EmailStr]
    course: Optional[str]
    gpa: Optional[float]

    class Config:
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}
        schema_extra = {
            "example": {
                "name": "Jane Doe",
                "email": "jdoe@example.com",
                "course": "Experiments, Science, and Fashion in Nanophotonics",
                "gpa": "3.0",
            }
        }


class MongoObject(BaseModel):
    # TODO: Find out how to encode ObjectId fields from str to ObjectId when saving to Mongo
    class Config:
        allow_population_by_field_name = True
        arbitrary_types_allowed = True
        orm_mode = True
        json_encoders = {ObjectId: str}

    id: PyObjectId = Field(default_factory=PyObjectId, alias="_id")


class Prazo(BaseModel):
    Prazo: datetime = None
    TipoId: PyObjectId
    Tipo: str
    TipoTermo: PyObjectId
    SubtipoId: PyObjectId = None
    SubTipo: str = None


class Publication(BaseModel):
    class Config:
        allow_population_by_field_name = True
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}

    id: PyObjectId = Field(default_factory=PyObjectId, alias="_id")
    # Default value None makes a field optional
    # Type annotations are used as schema validation
    ProcessoId: PyObjectId = Field(...)
    Data: datetime = Field(...)
    Conteudo: str = Field(...)
    DataCadastro: datetime = Field(...)
    Hash: str = None
    Observacao: str = None
    IdPubAutomacao: PyObjectId = None
    IdPubAutomacaoOrigem: PyObjectId = None
    IdPubDescartada: PyObjectId = None
    IdPublicacaoOrigem: PyObjectId = None
    DiarioCodigo: int = None
    IntimacaoEletronica: bool = None
    IsAvulsa: bool = None
    Pagina: int = None
    PaginaTermino: int = None
    Prazos: Optional[List[Prazo]] = None
    CodigosDestaque: str = None
    Metadados: List[Any] = None
    ChaveJuridicaStr: str = None
    NumeroCNJ: str = None
    NumerosProcessosConteudo: List[str] = None
    AuditoriaId: int = None
    Tipos: List[str] = None
    Solr: Any = None
    DataTipagem: datetime = None


class PublicationUpdate(MongoObject):
    class Config:
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}

    ProcessoId: PyObjectId = None
    Conteudo: str = None
    Hash: str = None
    Observacao: str = None
    IdPubAutomacao: PyObjectId = None
    IdPubAutomacaoOrigem: PyObjectId = None
    IdPubDescartada: PyObjectId = None
    IdPublicacaoOrigem: PyObjectId = None
    DiarioCodigo: int = None
    IntimacaoEletronica: bool = None
    IsAvulsa: bool = None
    Pagina: int = None
    PaginaTermino: int = None
    Prazos: Optional[List[Prazo]] = None
    CodigosDestaque: str = None
    Metadados: List[Any] = None
    ChaveJuridicaStr: str = None
    NumeroCNJ: str = None
    NumerosProcessosConteudo: List[str] = None
    AuditoriaId: int = None
    Tipos: List[str] = None
    Solr: Any = None
    DataTipagem: datetime = None

import os

import motor.motor_asyncio
from dotenv import load_dotenv

load_dotenv()

MONGODB_URI = os.environ["MONGODB_URI"]
client = motor.motor_asyncio.AsyncIOMotorClient(MONGODB_URI)
db_college = client.college  # Creates a db named college - wtf
bigDataV2 = client.bigDataV2
classificadorBigdata = client.classificadorBigdata
PUBLICATION_COLLECTION_NAME = "testePublicationApi"
publication_collection = classificadorBigdata[PUBLICATION_COLLECTION_NAME]
